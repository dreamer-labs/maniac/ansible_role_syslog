# Role Name

This role installs and configures rules for `syslog-ng`

## Requirements

See `requirements.yml` for role requirements.

## Role Variables

Specify `syslog-ng` rules as follows:

```
roles:
  - role: ansible_role_syslog
    syslog_sources:
      - name: auditd
        type: file
        config: "\"/var/log/audit/audit.log\" flags(no-parse)"
    syslog_destinations:
      - name: graylog
        type: syslog
        config: "\"{{ graylog_server }}\" port(514)"
    syslog_log_items:
      - source: auditd
        destination: graylog
```

## Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in
regards to parameters that may need to be set for other roles, or variables that
are used from other roles.

## Example Playbook

See `molecule/with-graylog-server/converge.yml` for a working example

## License

BSD

from io import StringIO
import time
import csv
import os
import json
import pytest
import uuid

import testinfra
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("graylog")


@pytest.fixture
def generate_log():
    testinfra_host = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ["MOLECULE_INVENTORY_FILE"]
    ).get_hosts("syslog")[0]

    host = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ["MOLECULE_INVENTORY_FILE"]
    ).get_host(testinfra_host)

    msg = f"pytest_molecule_{str(uuid.uuid4())}"

    host.run(f"logger {msg}")

    time.sleep(2)

    return msg


def test_log_sends(host, generate_log):
    username = "admin"
    password = "yourpassword"

    msg = generate_log

    body_json = {
        "timerange": {"type": "relative", "range": 1000},
        "query_string": {"type": "elasticsearch", "query_string": msg},
    }
    cmd = f"""
        curl -X "POST" http://localhost:9000/api/views/search/messages \
            -H "Content-Type: application/json" \
            -H "X-Requested-By: Molecule" \
            -u "{username}:{password}" \
            -d '{json.dumps(body_json)}'
        """

    logs = []
    output = host.run(cmd).stdout

    csvreader = csv.reader(StringIO(output))
    for row in csvreader:
        logs.append(row)

    # 3rd item in array is the message
    assert any(msg in log[2] for log in logs)

---
- name: Wait for SSH connection
  hosts: all
  gather_facts: false
  tasks:
    - name: Wait 60 seconds
      wait_for_connection:
        timeout: 60


- name: Prepare
  hosts: graylog
  become: true
  pre_tasks:
    - name: Install required packages
      package:
        name:
          - firewalld
          - python-firewall
          - iproute
          - which

    - name: Start firewalld
      service:
        name: firewalld
        state: started

- name: Configure Graylog
  hosts: graylog
  become: true
  vars:
    cert_name: test_selfsigned
    molecule_run_dir: "{{ lookup('env', 'MOLECULE_EPHEMERAL_DIRECTORY') }}"
  pre_tasks:
    - block:
        - name: Create temporary dir for run
          file:
            path: "{{ molecule_run_dir }}/tmp"
            state: directory
          register: cert_tmpdir

        - name: Create CA key
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/CA_key.pem"
          register: ca_key

        - name: Create the CA CSR
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/CA.csr"
            privatekey_path: "{{ ca_key.filename }}"
            basic_constraints:
              - CA:TRUE
            common_name: "my-ca"
          register: ca_csr

        - name: sign the CA CSR
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/CA.crt"
            csr_path: "{{ ca_csr.filename }}"
            privatekey_path: "{{ ca_key.filename }}"
            provider: selfsigned
          register: ca_crt

        - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.pem"
          register: test_key

        - name: Generate an OpenSSL Certificate Signing Request
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.csr"
            privatekey_path: "{{ test_key.filename }}"
            common_name: graylog.example.com
            subject_alt_name:
              - "IP:{{ hostvars[inventory_hostname]['ansible_eth0']['ipv4']['address'] }}"
              - "IP:127.0.0.1"
              - "DNS:localhost"
          register: test_csr

        - name: Generate a Self Signed OpenSSL certificate
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.crt"
            privatekey_path: "{{ test_key.filename }}"
            csr_path: "{{ test_csr.filename }}"
            ownca_path: "{{ ca_crt.filename }}"
            ownca_privatekey_path: "{{ ca_key.filename }}"
            provider: ownca
          register: test_crt

      run_once: true
      become: false
      delegate_to: localhost

    - name: Copy openSSL files
      copy:
        src: "{{ item.file }}"
        dest: "{{ item.dest }}"
      loop:
        - dest: "/etc/pki/tls/certs/{{ test_crt.filename | basename }}"
          file: "{{ test_crt.filename }}"
        - dest: "/etc/pki/tls/private/{{ test_key.filename | basename }}"
          file: "{{ test_key.filename }}"
        - dest: "/etc/pki/ca-trust/source/anchors/{{ ca_crt.filename | basename }}"
          file: "{{ ca_crt.filename }}"

    - name: Update ca trust
      command: |
        update-ca-trust extract
        update-ca-trust enable
      changed_when: False
      become: True

    - name: Open port 1514
      ansible.posix.firewalld:
        port: "{{ item }}"
        permanent: True
        state: enabled
        immediate: True
      loop:
        - "1514/udp"

    - include_role:
        name: ansible_role_graylog
      vars:
        graylog_password_secret: secretsecretsecretsecret
        graylog_root_password_sha2: e3c652f0ba0b4801205814f8b6bc49672c4c74e25b497770bb89b22cdeb4e951
        graylog_http_publish_uri: "https://{{ hostvars[inventory_hostname]['ansible_eth0']['ipv4']['address'] }}/"
        graylog_http_external_uri: "https://{{ hostvars[inventory_hostname]['ansible_eth0']['ipv4']['address']}}/"
        graylog_http_enable_tls: True
        graylog_http_tls_cert_file: "/etc/pki/tls/certs/{{ test_crt.filename | basename }}"
        graylog_http_tls_key_file: "/etc/pki/tls/private/{{ test_key.filename | basename }}"
        graylog_selinux_enable: True
        graylog_server_version: 3.3.10
        es_version: 6.8.13
        graylog_server_heap_size: "1000m"

    - name: Add syslog input
      dreamer_labs.graylog_modules.graylog_input_rsyslog:
        endpoint: "127.0.0.1"
        graylog_user: "admin"
        graylog_password: "yourpassword"
        validate_certs: "false"
        action: "create"
        input_type: "UDP"
        title: "Rsyslog UDP"
        global_input: "true"
        allow_override_date: "true"
        bind_address: "0.0.0.0"
        expand_structured_data: "false"
        force_rdns: "false"
        number_worker_threads: "2"
        port: "1514"
        recv_buffer_size: "1048576"
        store_full_message: "true"
